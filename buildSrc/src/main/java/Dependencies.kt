const val kotlinVersion = "1.3.50"
const val roomVersion = "2.2.1"

object BuildPlugins {
    object Versions {
        const val buildToolsVersion = "3.5.1"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.buildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
    const val kotlinKapt = "kotlin-kapt"
}

object AndroidSdk {
    const val min = 19
    const val compile = 29
    const val target = compile
}

object AndroidLibraries {
    private object Versions {
        const val appCompat = "1.1.0"
        const val support = "1.0.0"
        const val material = "1.0.0"
        const val cardView = "1.0.0"
        const val recyclerView = "1.0.0"
        const val core = "1.0.0"
        const val lifecycle = "2.1.0"
        const val constraintLayout = "1.1.3"
    }

    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val support = "androidx.legacy:legacy-support-v4:${Versions.support}"
    const val design = "com.google.android.material:material:${Versions.material}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardView}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val core = "androidx.core:core-ktx:${Versions.core}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    const val extensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    const val reactiveStreams =
        "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.lifecycle}"
}

object NetworkLibraries {
    private object Versions {
        const val retrofit = "2.5.0"
        const val retrofitRxJava = "1.0.0"
        const val moshi = "1.8.0"
        const val glide = "4.10.0"
    }

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val retrofitRxJava = "com.jakewharton.retrofit:retrofit2-rxjava2-adapter:${Versions.retrofitRxJava}"
    const val moshi = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    const val moshiProcessor = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    const val glideIntegration = "com.github.bumptech.glide:okhttp3-integration:${Versions.glide}"

    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:3.10.0"
}

object RxJava {
    private object Versions {
        const val rxJava = "2.2.8"
        const val rxAndroid = "2.1.1"
    }
    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
}

object Room {
    const val roomRuntime = "androidx.room:room-runtime:$roomVersion"
    const val roomCompiler = "androidx.room:room-compiler:$roomVersion"
    const val roomRxjava = "androidx.room:room-rxjava2:$roomVersion"
}

object Dagger {
    private object Versions {
        const val dagger = "2.25"
        const val assistedInject = "0.5.0"
    }

    const val dagger = "com.google.dagger:dagger-android:${Versions.dagger}"
    const val daggerSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerAndroidProcessor =
        "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val assistedInjectAnnotation =
        "com.squareup.inject:assisted-inject-annotations-dagger2:${Versions.assistedInject}"
    const val assistedInjectProcessor =
        "com.squareup.inject:assisted-inject-processor-dagger2:${Versions.assistedInject}"
    const val javaXannotation = "javax.annotation:jsr250-api:1.0"
    const val inject = "javax.inject:javax.inject:1"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.12"
        const val testRunner = "1.1.0-alpha4"
        const val espresso = "3.1.0-alpha4"
        const val robolectric = "4.3"
        const val mockito = "2.2.0"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val core = "androidx.test:core:1.2.0"
    const val roomTest = "androidx.room:room-testing:$roomVersion"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"
    const val mockito = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockito}"
}

object OtherLibraries {


    const val threetenabp = "com.jakewharton.threetenabp:threetenabp:1.1.0"
}