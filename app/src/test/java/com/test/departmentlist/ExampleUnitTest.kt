package com.test.departmentlist

import androidx.lifecycle.ViewModelProviders
import com.test.departmentlist.view.MainActivity
import com.test.departmentlist.viewmodel.MainViewModel
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@Config(application = DummyApplication::class)
@RunWith(RobolectricTestRunner::class)
class ExampleUnitTest {

    @Test
    fun notificationCount() {
        val activity = Robolectric.buildActivity(MainActivity::class.java).create()
        val vm = ViewModelProviders.of(activity.get())[MainViewModel::class.java]
    }
}
