package com.test.departmentlist.utils

import java.util.Calendar
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

object DateTimeConverter {

    fun localDatetime(milliseconds: Long): LocalDateTime {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(milliseconds), ZoneId.systemDefault())
    }

    fun milliseconds(localDateTime: LocalDateTime): Long {
        val zoneId = ZoneId.systemDefault()
        return localDateTime.atZone(zoneId).toEpochSecond()
    }

    fun getUTCZonedDateTime(localDateTime: LocalDateTime): ZonedDateTime {
        return localDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC)
    }

    fun localDate(dateString: String): LocalDateTime {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        return LocalDateTime.parse(dateString, formatter)
    }

    fun localISODate(dateString: String): LocalDateTime {
        val formatter = DateTimeFormatter.ISO_DATE_TIME
        return LocalDateTime.parse(dateString, formatter)
    }

    fun getCalendar(localDateTime: LocalDateTime): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(
            localDateTime.year,
            localDateTime.monthValue,
            localDateTime.dayOfMonth,
            localDateTime.hour,
            localDateTime.minute
        )
        return calendar
    }

    @JvmStatic
    fun getReadableTime(localDateTime: LocalDateTime): String {
        val formatter = DateTimeFormatter.ofPattern("EEEE, d. MMMM HH:mm")
        return localDateTime.format(formatter)
    }

    @JvmStatic
    fun getTimeRange(startTime: LocalDateTime, endTime: LocalDateTime): String {
        var formatter = DateTimeFormatter.ofPattern("EEEE, d. MMMM")
        return if (startTime.dayOfMonth != endTime.dayOfMonth && endTime.hour != 0) {
            val result = startTime.format(formatter) + " - " + endTime.format(formatter) + "\n"
            formatter = DateTimeFormatter.ofPattern("HH:mm")
            result + (startTime.format(formatter) + " - " + endTime.format(formatter))
        } else {
            val result = startTime.format(formatter) + "\n"
            formatter = DateTimeFormatter.ofPattern("HH:mm")
            result + (startTime.format(formatter) + " - " + endTime.format(formatter))
        }
    }

    @JvmStatic
    fun getTimeRangeForMap(startTime: LocalDateTime, endTime: LocalDateTime): String {
        var formatter = DateTimeFormatter.ofPattern("EEEE, d. MMMM")
        val result = startTime.format(formatter) + ", "
        formatter = DateTimeFormatter.ofPattern("HH:mm")

        return result + (startTime.format(formatter) + " - " + endTime.format(formatter))
    }

    fun getReadableDate(date: LocalDateTime): String {
        val formatter = DateTimeFormatter.ofPattern("d. MMMM")
        return date.format(formatter)
    }
}
