package com.test.departmentlist.utils

enum class RepositoryState {
    READY, LOADING, ERROR
}
