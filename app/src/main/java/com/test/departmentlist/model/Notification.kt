package com.test.departmentlist.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDateTime

@Parcelize
@Entity(tableName = "notifications")
data class Notification(
    @PrimaryKey(autoGenerate = false)
    var id: Long = 0,
    @ColumnInfo(name = "title")
    var title: String = "",
    @ColumnInfo(name = "text")
    var text: String = "",
    @ColumnInfo(name = "type")
    var type: Int = 0,
    @ColumnInfo(name = "session")
    var session: Int = 0,
    @ColumnInfo(name = "remindAtTime")
    var remindAtTime: LocalDateTime,
    @ColumnInfo(name = "time")
    var time: LocalDateTime
) : Parcelable
