package com.test.departmentlist.model

import androidx.lifecycle.MutableLiveData
import com.test.departmentlist.network.ApiService
import com.test.departmentlist.room.NotificationDao
import com.test.departmentlist.utils.RepositoryState
import io.reactivex.Flowable
import javax.inject.Singleton

@Singleton
class NotificationRepository(
    private val notificationDao: NotificationDao,
    private val apiService: ApiService
) : INotificationRepository {
    override val state: MutableLiveData<RepositoryState> = MutableLiveData()

    init {
        state.postValue(RepositoryState.LOADING)
    }

    override fun downloadData(): Flowable<Boolean> {
        state.postValue(RepositoryState.LOADING)

        return apiService.getNotifications().map { response ->
            if (response.isSuccessful) {
                response.body()?.let {
                    notificationDao.updateData(it)
                    state.postValue(RepositoryState.READY)
                }

                true
            } else {

                false
            }
        }.onErrorReturn {
            false
        }
    }

    override fun getNotifications(): Flowable<List<Notification>> {
        return notificationDao.getNotificationCountRx()
                .take(1)
                .flatMap { count ->
                    if (count.isEmpty() || count[0] == 0) {
                        apiService.getNotifications()
                                .map { response ->
                                    if (response.isSuccessful) {
                                        response.body()?.let {
                                            notificationDao.updateData(it)
                                        }
                                    }

                                    state.postValue(RepositoryState.READY)
                                }.ignoreElements()
                                .andThen(notificationDao.getAllRx())
                    } else {
                        state.postValue(RepositoryState.READY)
                        notificationDao.getAllRx()
                    }
                }
    }

    override fun getNotification(id: Long): Flowable<List<Notification>> {
        return notificationDao.getNotification(id)
    }
}
