package com.test.departmentlist.model

import androidx.lifecycle.MutableLiveData
import com.test.departmentlist.utils.RepositoryState
import io.reactivex.Flowable

interface INotificationRepository {
    val state: MutableLiveData<RepositoryState>

    fun downloadData(): Flowable<Boolean>
    fun getNotifications(): Flowable<List<Notification>>
    fun getNotification(id: Long): Flowable<List<Notification>>
}
