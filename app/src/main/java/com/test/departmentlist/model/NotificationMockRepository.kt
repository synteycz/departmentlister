package com.test.departmentlist.model

import androidx.lifecycle.MutableLiveData
import com.test.departmentlist.utils.RepositoryState
import io.reactivex.Flowable
import org.threeten.bp.LocalDateTime

class NotificationMockRepository : INotificationRepository {
    override val state: MutableLiveData<RepositoryState> = MutableLiveData()

    override fun downloadData(): Flowable<Boolean> {
        return Flowable.just(false)
    }

    override fun getNotifications(): Flowable<List<Notification>> {
        val notification =
            Notification(1, "title", "text", 2, 1, LocalDateTime.now(), LocalDateTime.now())

        return Flowable.just(listOf(notification))
    }

    override fun getNotification(id: Long): Flowable<List<Notification>> {
        val notification =
            Notification(1, "title", "text", 2, 1, LocalDateTime.now(), LocalDateTime.now())

        return Flowable.just(listOf(notification))
    }
}
