package com.test.departmentlist.di

import com.test.departmentlist.view.NotificationDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NotificationDetailFragmentModule {

    @ContributesAndroidInjector
    abstract fun notificationDetailFragment(): NotificationDetailFragment
}
