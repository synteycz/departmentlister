package com.test.departmentlist.di

import android.app.Application
import androidx.room.Room
import com.test.departmentlist.room.DB
import com.test.departmentlist.room.NotificationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DBModule {

    @Singleton
    @Provides
    fun provideDatabase(application: Application): DB {
        return Room.databaseBuilder(application,
                DB::class.java, "database.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideNotificationDao(db: DB): NotificationDao {
        return db.notificationDao()
    }
}
