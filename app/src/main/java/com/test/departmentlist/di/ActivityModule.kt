package com.test.departmentlist.di

import com.test.departmentlist.view.MainActivity
import com.test.departmentlist.view.NotificationDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [NotificationDetailFragmentModule::class])
    abstract fun contributeNotificationDetailActivity(): NotificationDetailActivity
}
