package com.test.departmentlist.di

import com.test.departmentlist.model.INotificationRepository
import com.test.departmentlist.model.NotificationRepository
import com.test.departmentlist.network.ApiService
import com.test.departmentlist.room.NotificationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NotificationModule {

    @Provides
    @Singleton
    fun provideNotificationRepository(
        notificationDao: NotificationDao,
        apiService: ApiService
    ): INotificationRepository {
        return NotificationRepository(notificationDao, apiService)
    }
}
