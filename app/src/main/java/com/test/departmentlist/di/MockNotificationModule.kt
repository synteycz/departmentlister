package com.test.departmentlist.di

import com.test.departmentlist.model.INotificationRepository
import com.test.departmentlist.model.NotificationMockRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MockNotificationModule {

    @Provides
    @Singleton
    fun provideNotificationRepository(): INotificationRepository {
        return NotificationMockRepository()
    }
}
