package com.test.departmentlist.di

import android.app.Application
import com.test.departmentlist.DaggerTest
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [

    AndroidSupportInjectionModule::class,
    ViewModelModule::class,
    NetworkModule::class,
    DBModule::class,
    ActivityModule::class,
    NotificationDetailFragmentModule::class,
    NotificationModule::class
])
@Singleton
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    fun inject(application: DaggerTest)
}
