package com.test.departmentlist.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.departmentlist.R
import com.test.departmentlist.databinding.ActivityMainBinding
import com.test.departmentlist.model.Notification
import com.test.departmentlist.utils.RepositoryState
import com.test.departmentlist.view.adapter.NotificationAdapter
import com.test.departmentlist.viewmodel.MainViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: NotificationAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        adapter = NotificationAdapter { notification -> itemClicked(notification) }

        binding.notificationList.setHasFixedSize(true)
        binding.notificationList.layoutManager = LinearLayoutManager(this)

        binding.notificationList.adapter = adapter

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.notifications.observe(this, Observer { notifications ->
            adapter.updateData(notifications)
        })

        viewModel.repositoryState.observe(this, Observer { state ->
            when (state) {
                RepositoryState.READY -> {
                    binding.loadingPanel.visibility = View.GONE
                    binding.warningPanel.visibility = View.GONE
                }
                RepositoryState.LOADING -> {
                    binding.loadingPanel.visibility = View.VISIBLE
                    binding.warningPanel.visibility = View.GONE
                }
                RepositoryState.ERROR -> {
                    binding.warningPanel.visibility = View.VISIBLE
                    binding.loadingPanel.visibility = View.GONE
                }
            }
        })
    }

    private fun itemClicked(notification: Notification) {
        val intent = Intent(this, NotificationDetailActivity::class.java)
        intent.putExtra("notificationId", notification.id)
        startActivity(intent)
    }
}
