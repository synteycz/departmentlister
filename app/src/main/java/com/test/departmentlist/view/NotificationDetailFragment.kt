package com.test.departmentlist.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.test.departmentlist.R
import com.test.departmentlist.databinding.FragmentNotificationDetailBinding
import com.test.departmentlist.model.INotificationRepository
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class NotificationDetailFragment : Fragment() {

    @Inject
    lateinit var repository: INotificationRepository

    private lateinit var binding: FragmentNotificationDetailBinding

    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun onAttach(context: Context) {
        super.onAttach(context)

        AndroidSupportInjection.inject(this)

        val id = arguments?.getLong("notificationId") ?: 0

        disposable.add(
            repository.getNotification(id).observeOn(AndroidSchedulers.mainThread()).subscribeOn(
                Schedulers.io()
            ).subscribe {
                binding.data = it.firstOrNull()
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_notification_detail,
            container,
            false
        )

        return binding.root
    }

    override fun onDestroyView() {
        disposable.clear()

        super.onDestroyView()
    }

    companion object {
        fun newInstance(id: Long): NotificationDetailFragment {
            val fragment = NotificationDetailFragment()
            val bundle = Bundle()
            bundle.putLong("notificationId", id)
            fragment.arguments = bundle
            return fragment
        }
    }
}
