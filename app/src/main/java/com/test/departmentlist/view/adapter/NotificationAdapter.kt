package com.test.departmentlist.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.departmentlist.databinding.NotificationItemBinding
import com.test.departmentlist.model.Notification

// https://stackoverflow.com/questions/44549250/initializing-a-recyclerview-with-android-data-binding-in-kotlin-is-throwing-an-e
// https://android.jlelse.eu/recyclerview-with-android-data-binding-cc2e5aefd53a
class NotificationAdapter(
    private val clickListener: (Notification) -> Unit
) : RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {

    private val items = mutableListOf<Notification>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(
            NotificationItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bind(items[position], clickListener)
    }

    fun updateData(notifications: List<Notification>?) {
        items.clear()
        if (notifications != null) {
            items.addAll(notifications)
        }
        notifyDataSetChanged()
    }

    class NotificationViewHolder(private val binding: NotificationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(notification: Notification, clickListener: (Notification) -> Unit) {
            binding.data = notification
            binding.root.setOnClickListener { clickListener(notification) }
        }
    }
}
