package com.test.departmentlist.network

import com.test.departmentlist.model.Notification
import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("notifications/99")
    fun getNotifications(): Flowable<Response<List<Notification>>>
}
