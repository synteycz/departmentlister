package com.test.departmentlist.network

import com.squareup.moshi.FromJson
import com.test.departmentlist.utils.DateTimeConverter
import org.threeten.bp.LocalDateTime

class DateAdapter {
    @FromJson
    fun fromJson(date: String): LocalDateTime {
        return DateTimeConverter.localDate(date)
    }
}
