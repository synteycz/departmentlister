package com.test.departmentlist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.ViewModel
import com.test.departmentlist.model.INotificationRepository
import com.test.departmentlist.model.Notification
import com.test.departmentlist.utils.RepositoryState
import javax.inject.Inject

class MainViewModel @Inject constructor(
    repository: INotificationRepository
) : ViewModel() {

    val notifications: LiveData<List<Notification>> =
        LiveDataReactiveStreams.fromPublisher(repository.getNotifications())
    val repositoryState: LiveData<RepositoryState> = repository.state
}
