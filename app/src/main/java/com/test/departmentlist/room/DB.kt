package com.test.departmentlist.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.test.departmentlist.model.Notification

@Database(entities = [(Notification::class)], version = 2)
@TypeConverters(RoomConverters::class)
abstract class DB : RoomDatabase() {
    abstract fun notificationDao(): NotificationDao
}
