package com.test.departmentlist.room

import androidx.room.TypeConverter
import com.test.departmentlist.utils.DateTimeConverter
import org.threeten.bp.LocalDateTime

object RoomConverters {
    @TypeConverter
    @JvmStatic
    fun toDate(date: String?): LocalDateTime? {
        return if (date == null) null else DateTimeConverter.localISODate(date)
    }

    @TypeConverter
    @JvmStatic
    fun toString(localDateTime: LocalDateTime?): String? {
        return localDateTime?.toString()
    }
}
