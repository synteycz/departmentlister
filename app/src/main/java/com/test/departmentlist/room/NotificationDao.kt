package com.test.departmentlist.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.test.departmentlist.model.Notification
import io.reactivex.Flowable

@Dao
interface NotificationDao {

    @Query("SELECT * FROM notifications")
    fun getAllRx(): Flowable<List<Notification>>

    @Query("SELECT COUNT(*) FROM notifications")
    fun getNotificationCountRx(): Flowable<List<Int>>

    @Query("SELECT * FROM notifications")
    fun getList(): List<Notification>

    @Query("DELETE FROM notifications")
    fun deleteAll()

    @Query("SELECT * FROM notifications WHERE id = :id")
    fun getNotification(id: Long): Flowable<List<Notification>>

    @Transaction
    fun updateData(notifications: List<Notification>) {
        deleteAll()
        insert(notifications)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: Notification)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(obj: List<Notification>)
}
