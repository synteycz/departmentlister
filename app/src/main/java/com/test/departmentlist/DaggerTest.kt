package com.test.departmentlist

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.test.departmentlist.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class DaggerTest : Application(), HasAndroidInjector {
    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.factory()
            .create(this)
            .inject(this)

        AndroidThreeTen.init(this)
    }
}
